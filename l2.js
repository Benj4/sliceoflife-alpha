
function L(){
  var self = this;
  
  this.body = {};
  
  this.memory = {};

  this.position = {
    actual: {x:0, y:0, z:0},
    prev: {x:0, y:0, z:0},
    set: function(position){
      this.prev = this.actual;
      this.actual = position;
      
      //TODO
      //verificar triggers
    }
  };

  this.actions = {
    /**
     * 
     * Mueve a un punto especifico
     * @param {Object} position {x:1, y:2, x:3}
     * @returns {undefined}
     */
    move : function(position){
      self.position.set(position);
    },
    
    /**
     * 
     * Mueve una distancia especifica hacia una direccion
     * @param {Object} direction {angel:180, distance:10}
     * @returns {undefined}
     */
    move_direction : function(direction){
      
    },
    
    /**
     * 
     * @param {int} min distancia minima
     * @param {int} max distancia maxima
     * @returns {undefined}
     */
    move_random : function(min, max){
      var position_new = {
        x: god.random() + self.position.actual.x,
        y: god.random() + self.position.actual.y,
        z: god.random() + self.position.actual.z
      };
      
      this.move( position_new );
      
    },
    
    /**
     * 
     * Alias de actions.move()
     */
    move_position : function(position){
      this.move(position);
    }
  };
  
  this.adn = {
    body : {},
    actions : {},
    triggers : {}
  };
  
}

god = {
  
  random : function(){
    return Math.floor( (Math.random() * 10) +1 );
  }
  
};

